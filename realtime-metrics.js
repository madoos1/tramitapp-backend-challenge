const { connect, emitRealtimeMetrics } = require('./src/realtime-metrics')
const { write, writeSameLine } = require('./src/util')
const { stringify } = JSON

const PORT = 7000

const connection$ = connect(PORT)
const metrics$ = emitRealtimeMetrics(connection$)

connection$.subscribe((client) => write(`Client ${client.id} connected!`))
metrics$.subscribe((metric) => writeSameLine(`Sending metrics ${stringify(metric)}`))