const { join } = require('path')
const { withSpinner } = require('./src/util')
const downloadFile = require('./src/download-file')
const download = withSpinner('Downloading password file', downloadFile)

const SRC = "https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/xato-net-10-million-passwords.txt"
const DEST = join(__dirname, './passwords.txt')

download(SRC, DEST)