const { join } = require('path')
const { withExecutionTime, withSpinner, withMemoryUsage, pipe, then, show, write, question } = require('./src/util')

const checkPassword = require('./src/check-password')

// (* -> a) -> (* -> a)
const withFeedback = pipe(
    withExecutionTime, 
    withSpinner('checking password'),
    withMemoryUsage
)

// check :: (Password, Path) -> Promise Error ()
const check = pipe(
    withFeedback(checkPassword),
    then(show), 
    then((answer) => write(`Password in file: ${answer}`))
)

// check :: (Password, Path) -> Promise Error ()
const interactiveCheck = async (file) => {
    write('-'.repeat(30)) 
    const expectedPassword = await question('Check password: ')
    await check(expectedPassword, file)
    await interactiveCheck(file)
}

// main :: () -> ()
const main = () => {
    const PASSWORDS_FILE = join(__dirname, './passwords.txt')
    const EXPECTED_PASSWORD = process.argv[2]
    EXPECTED_PASSWORD ? check(EXPECTED_PASSWORD, PASSWORDS_FILE) : interactiveCheck(PASSWORDS_FILE)  // if the password to search is not passed, it goes into interactive mode 
}

main()




