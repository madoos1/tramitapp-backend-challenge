# TramitApp backend challenge

Crear una función que compruebe si la contraseña elegida por un usuario está en la lista de contraseñas más utilizadas:

https://github.com/danielmiessler/SecLists/blob/master/Passwords/xato-net-10-million-passwords.txt

La idea es que esa función reciba como entrada la contraseña a comprobar y que devuelva true/false según corresponda. Suponiendo que la función se puede invocar miles de veces en cortos periodos de tiempo y que queremos optimizar el consumo de memoria, el fichero de contraseñas debe de estar descargado en un fichero local y manejarse con un stream.

La contraseña a buscar se recibirá como argumento en la invocación del programa es decir, algo así:

node password_checker.js 12345

Donde password_checker.js será el programa js que busque a través de un stream la contraseña 12345 en  el fichero local previamente descargado.

No se pueden utilizar librerías de terceros (sólamente el api de node)

## Consideraciones

Este challenge ha sido implementado con metodología TDD.

En este repo se implementa el password checker en dos formas, una según especifica el ejercicio y otra interactiva para poder hacer consultas de passwords de una forma más sencilla y obtener información sobre tiempo de ejecución, memoria consumida, etc. Dado que el fichero de contraseñas tiene un peso considerable será descargado mediante un hook en el script postinstall para evitar añadirlo al repo. Si el proceso de descarga falla, ejecute `node download-passwords.js` o descargarlo manualmente y añadirlo a la raíz del proyecto con el nombre passwords.txt.

Por otro lado, implementa un servicio de métricas en tiempo real con socketio utilizado para el [TramitApp Front-End challenge #3](https://bitbucket.org/madoos1/tramitapp-front-challenge-3/src).

## Lanzar el check-password

* npm install (al terminar mediante un hook descargará el fichero de contraseñas)
* npm test (opcionalmente si se quiere ver covertura)
* ejecutar `node check-password <PASSWORD>`

Opcionalmente para hacer multiples pruebas se puede invocar directamente `node check-password` sin pasarle la contraseña para que pase a modo interactivo.

## Solución

Se implementa el método checkPassword usando el API nativa de Streams. 

* Para ganar performance y no tener que parsear cada línea del fichero de contraseñas se utiliza createInterface junto con createReadStream.
* Dado que los streams implementan el protocolo Iterator nos basamos en él para buscar de forma lineal la contraseña.
* La respuesta del método se expone como promesa para facilitar su consumo.

```js
const { createReadStream } = require('fs')
const { createInterface }  = require('readline');

// checkPassword :: (Password, Path) -> Promise Error Boolean
const checkPassword = async (expectedPassword, file) => {
    const passwords =  createInterface({ input: createReadStream(file) })

    for await (const password of passwords) {
        if(expectedPassword === password) {
            passwords.close()
            return true
        }
    }

    return false
}

module.exports = checkPassword
```

En el main del programa se utilizan patrones de composición funcional para decorar el método checkPassword para que nuestra un spinner mientras busca la contraseña, información sobre el tiempo de ejecución y memoria usada.

```js
const { join } = require('path')
const { withExecutionTime, withSpinner, withMemoryUsage, pipe, then, show, write } = require('./src/util')

const checkPassword = require('./src/check-password')

// (* -> a) -> (* -> a)
const withFeedback = pipe(
    withExecutionTime, 
    withSpinner('checking password'),
    withMemoryUsage
)

// check :: (Password, Path) -> Promise Error ()
const check = pipe(
    withFeedback(checkPassword),
    then(show), 
    then((answer) => write(`Password in file: ${answer}`))
)

```

## Ejemplo

![](example.gif)

## Ejemplo en modo interactivo

![](example-interactive.gif)

