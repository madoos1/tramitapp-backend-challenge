const { EOL } = require('os')
const { createInterface } = require('readline')

// curry :: (* -> a) -> (* -> a)
const curry = (f) => {
    const curried = (...args) => args.length >= f.length ? f(...args): (...rest) => curried(...args, ...rest)
    return curried
}

// pipe :: ((a, b, ..., n) -> o, o -> p, ..., q -> r) -> (a, b, ..., n) -> r
const pipe = (...fns) => fns.reduce(
    (f, g) => (...args) => g(f(...args))
)

// tap :: (a -> *) → a -> a
const tap = f => x => (f(x), x)

// then :: a -> b -> Promise Error a -> Promise Error b
const then = curry((f, p) => p.then(f) )

// show :: a -> String
const show = (x) => JSON.stringify(x)

// write :: String -> ()
const write = x => process.stdout.write(`${x}${EOL}`)

// writeSameLine :: String -> ()
const writeSameLine = (x = "") => process.stdout.write(`\r${x}`)

// withExecutionTime :: (*... -> Promise a) -> (*... -> Promise a)
const withExecutionTime = (f) => {
    return (...args) => {
        const start = process.hrtime()
        
        return f(...args).then(tap(() => {
            const end =  process.hrtime(start)
            writeSameLine(`Execution time: ${end[0]}s ${end[1] / 1000000}ms${EOL}`)
        }))
    }
}

// withSpinner :: String -> (*-> Promise a) -> (* -> Promise a)
const withSpinner = curry((msg, f) => {
    let position = 0
    const spinner = ["\\", "|", "/", "-"]
    const showSpinner = () => setInterval(() => (writeSameLine(`${spinner[position++]} ${msg}`),  position &= 3), 50)
    const cleanSpinner = () => writeSameLine()

    return (...args) => {
        const id = showSpinner()
        return f(...args).then(tap(() => (clearInterval(id), cleanSpinner())))
    }
})

// question :: String -> Promise Error String
const question = (question) => {
    const rl = createInterface({ input: process.stdin, output: process.stdout })
    return new Promise((resolve) => rl.question(question, (answer) => (rl.close(), resolve(answer))))
}

// withMemoryUsage :: (* -> Promise Error a) -> (* -> Promise Error a)
const withMemoryUsage = (f) => {
    return (...args) => {
        const usage = []
        const id = setInterval(() => usage.push(process.memoryUsage()), 250)
        return f(...args).then(tap(() => {
            usage.push(process.memoryUsage())
            clearInterval(id)
            const memory = usage.map(({heapTotal}) => heapTotal).reduce((x,y) => x + y, 0)
            const average = String((memory / usage.length) / 1000000)
            write(`Memory usage: ${average}MB`)
        }))        
    }
}

module.exports = {
    curry,
    pipe,
    tap,
    withExecutionTime,
    withSpinner,
    then,
    show,
    writeSameLine,
    write,
    question,
    withMemoryUsage
}
