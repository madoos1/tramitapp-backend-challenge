const { EOL } = require('os')
const { writeFileSync, rmSync } = require('fs')
const { join } = require('path')

const checkPassword = require('./check-password')

const PASSWORDS_FILE = join(__dirname, 'fake_passwords')

beforeAll(() => writeFileSync(PASSWORDS_FILE, ['123456', 'admin'].join(EOL)))

afterAll(() => rmSync(PASSWORDS_FILE))

test('checkPassword should return true when password exists in file', async () => {
    const isInFile = await checkPassword('123456', PASSWORDS_FILE)
    expect(isInFile).toEqual(true)
})

test('checkPassword should return false when password not exists in file', async () => {
    const isInFile = await checkPassword('none', PASSWORDS_FILE)
    expect(isInFile).toEqual(false)
})