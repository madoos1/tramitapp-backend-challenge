const { curry, pipe, tap, then, show, withExecutionTime, withSpinner } = require('./util')

test('curry should partial apply an function', () => {
    const add = curry((a, b) => a + b)
    expect(add(1)(1)).toEqual(2)
    expect(add(1, 1)).toEqual(2)
})

test('pipe compose functions', () => {
    const doublePlus = pipe(n => n + 1, n => n * 2)
    expect(doublePlus(1)).toEqual(4)
})

test('tap should return the same input and do a side effect', () => {
    const fn = jest.fn().mockImplementation(x => x);
    const log = tap(fn)
    expect(log(1)).toEqual(1)
    expect(fn).toHaveBeenCalled()
}) 

test('then should compose functions with Promise', async () => {
    const one = Promise.resolve(1)
    const plus = x => x+1
    const two = await then(plus, one)
    expect(two).toEqual(2)
})

test('show should return a String representation', () => {
    expect(show(true)).toEqual('true')
})

test('withExecutionTime should notify execution time', async () => {
    const plusP = jest.fn().mockImplementation(x => Promise.resolve(x+1))
    const stdoutWrite = jest.spyOn(process.stdout, 'write')

    const plusWithExecutionTime = withExecutionTime(plusP)

    const two = await plusWithExecutionTime(1)
    expect(two).toEqual(2)
    expect(plusP).toHaveBeenCalled()
    expect(stdoutWrite).toHaveBeenCalledWith(expect.stringContaining("Execution time:"))
    stdoutWrite.mockRestore()
})

test('withSpinner should show spinner until resolved promise', async () => {
    const plusP = jest.fn().mockImplementation(x => Promise.resolve(x+1))
    const stdoutWrite = jest.spyOn(process.stdout, 'write')

    const plusWithSpinner = withSpinner('plus message', plusP)
    const four = await plusWithSpinner(3)
    expect(four).toEqual(4)
    expect(plusP).toHaveBeenCalled()
    stdoutWrite.mockRestore()
})