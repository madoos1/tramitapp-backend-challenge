const { createReadStream } = require('fs')
const { createInterface }  = require('readline');

// checkPassword :: (Password, Path) -> Promise Error Boolean
const checkPassword = async (expectedPassword, file) => {
    const passwords =  createInterface({ input: createReadStream(file) })

    for await (const password of passwords) {
        if(expectedPassword === password) {
            passwords.close()
            return true
        }
    }

    return false
}

module.exports = checkPassword