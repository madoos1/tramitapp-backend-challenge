const https = require('https')
const { promisify } = require('util')
const pipeline = promisify(require('stream').pipeline)
const { createWriteStream } = require('fs')

// get :: URL -> Promise Error Response
const get = (url) => new Promise((resolve) => https.get(url,  resolve)) 

// downloadFile :: (URL, Path) -> Promise Error ()
const downloadFile = async (src, dest) => {
    const response = await get(src)
    await pipeline(response, createWriteStream(dest))
}

module.exports = downloadFile