const { join } = require('path')
const { rmSync, readFileSync } = require('fs')
const { Readable } = require('stream')
const { EOL } = require('os')

const https = require('https')
jest.mock('https');

const downloadFile = require('./download-file')

const PASSWORD_FILE = join(__dirname, 'downloaded-passwords.txt')
const passwords = ['123456', 'admin'].join(EOL)

beforeAll(() => https.get.mockImplementationOnce((_, cb) => cb(Readable.from(passwords))))

afterAll(() => rmSync(PASSWORD_FILE))

test('downloadFile should download file in stream', async() => {
    await downloadFile('https://passwords.txt', PASSWORD_FILE)
    const fileContent = readFileSync(PASSWORD_FILE, 'utf-8')
    expect(fileContent).toEqual(passwords)
})