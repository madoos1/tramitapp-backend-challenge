const createSocketConnection = require('socket.io')
const { fromEvent, interval } = require('rxjs')
const { mergeMap, map } = require('rxjs/operators')
const { pipe, tap } = require('./util')

const INTERVAL = 1000
const EVENT = 'metric'

// _randomBetween :: (Number, Number) -> Number
const _randomBetween = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)

// connect :: Number -> Stream socketIOClient
const connect = pipe(
  (port) => createSocketConnection(port, { cors: { origin: '*' }}),
  (io) => fromEvent(io, 'connection')
)

// createMetrics :: () -> Stream Metrics
const createMetrics = pipe(
  () => interval(INTERVAL),
  map(() => ({ "Date": `20${_randomBetween(19, 21)}-06-29T00:03:50.000Z`, "Close": _randomBetween(-1000, 2000) }))
)

// emitRealtimeMetrics :: Stream socketIOClient -> Stream Metric
const emitRealtimeMetrics = mergeMap(
  (socket) => createMetrics().pipe(map(tap((metric) => socket.emit(EVENT, metric))))
)

module.exports = {
  connect,
  emitRealtimeMetrics
}